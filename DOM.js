let increaseEl = document.getElementById("increase");
let decreaseEl = document.getElementById("decrease");
let onEl = document.getElementById("on");
let offEl = document.getElementById("off");
let resetEl = document.getElementById("reset");
let storeEl = document.getElementById("store");
let reloadEl = document.getElementById("reload");
let resultEl = document.getElementById("result");


let data = 0


function increase(e) {
    let value = parseInt(resultEl.textContent)
    value += 1
    resultEl.textContent = value
}

function decrease(e) {
    let value = parseInt(resultEl.textContent)
    value -= 1
    if(value < 0){
        alert("can't decrease below zero.")
    }
    else {
        resultEl.textContent = value
    }
}

function switchOn(e) {
  resultEl.textContent = 0
}

function reset(e) {
  resultEl.textContent = 0;
}

function switchOff(e) {
  resultEl.textContent = "";
}

function store(e) {
    data = parseInt(resultEl.textContent)
    if (data > 0 && data != null){
        localStorage.setItem("data", JSON.stringify(data));
        alert("Your data is stored successfully.");
    }
    else{
        alert("nothing to store")
    }
  
}

function reload(e) {
    data = parseInt(localStorage.getItem("data"));
    if (data > 0){
        resultEl.textContent = data
        alert("Your data is reloaded successfully.")
    }
    else{
        alert("You don't have any data to reload")
    }
}
